package ru.anvics.positive.feature.first_frag;

import android.content.Context;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

public class MyLinearSmoothScroller extends LinearSmoothScroller {

    private static final float MILLISECONDS_PER_INCH = 300f;

    public MyLinearSmoothScroller(Context context) {
        super(context);
    }

    @Override public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
        return boxStart - viewStart;
    }

    @Override protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
    }
    @Override
    public int calculateDyToMakeVisible(View view, int snapPreference) {
        int dy = super.calculateDyToMakeVisible(view, snapPreference);
        if (dy == 0) {
            return dy;
        }

        dy = adjustDyForUp(dy);
        return dy;
    }

    private int adjustDyForUp(int dy) {
        final RecyclerView.LayoutManager layoutManager = getLayoutManager();
        if (layoutManager == null || !layoutManager.canScrollVertically()) {
            return 0;
        }

        final View lastChild = layoutManager.getChildAt(layoutManager.getChildCount() - 1);
        final int position = layoutManager.getPosition(lastChild);
        if (position == layoutManager.getItemCount() - 1) {
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) lastChild.getLayoutParams();
            final int maxDy = layoutManager.getHeight() - layoutManager.getPaddingBottom()
                    - (layoutManager.getDecoratedBottom(lastChild) + params.bottomMargin);

            if (dy < maxDy) {
                return maxDy;
            }
        }
        return dy;
    }

}
