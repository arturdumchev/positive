package ru.anvics.positive.feature.common.view.view_pager

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup


open class BasePagerAdapter<T>(protected var items: List<T>) : PagerAdapter() {
    protected var TAG = BasePagerAdapter::class.java.simpleName

    fun resetImages(images: List<T>) {
        items = images
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        (container as ViewPager).removeView(view as View)
    }
}
