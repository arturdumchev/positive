package ru.anvics.positive.feature.common.view.view_pager

import android.content.Context
import android.view.animation.Interpolator
import android.widget.Scroller

class CustomScroller(context: Context, interpolator: Interpolator) : Scroller(context, interpolator) {

    companion object {
        private const val TAG = "CustomScroller"
    }

    val mDuration = 240

    override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
        // Ignore received duration, use fixed one instead
        super.startScroll(startX, startY, dx, dy, mDuration)
    }
}