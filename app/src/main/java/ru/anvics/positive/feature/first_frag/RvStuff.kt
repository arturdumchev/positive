package ru.anvics.positive.feature.first_frag


import android.view.View
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.first_item.view.*
import ru.anvics.positive.R
import ru.anvics.positive.extentions.eats
import ru.anvics.positive.feature.common.view.rv.BaseDelegateAdapter
import ru.anvics.positive.feature.common.view.rv.IRvItem
import ru.anvics.positive.feature.common.view.rv.ITEM_NEWS_ID


class ItemNews(val sDate: String, var sTitle: String, var sMessage: String) : IRvItem {
    override fun id(): Any = this
    override fun getViewType(): Int = ITEM_NEWS_ID
}

class SimpleViewHolder(
        private val doOnClick: (view: View, name: String) -> Unit,
        private val canGoNextSubj: BehaviorSubject<Boolean>
) : BaseDelegateAdapter<ItemNews>() {

    private val TAG = "RvStuff"
    val TRANS_NAME = "TRANS_NAME"

    override val itemLayoutId: Int get() = R.layout.first_item
    override fun View.onItemInflated(item: ItemNews) {
        tvDate.text = item.sDate
        tvTitle.text = item.sTitle
        tvMessage.text = item.sMessage

        tvTitle.transitionName = ""
        llTextsContainer.setOnClickListener {
            if (canGoNextSubj.value) {
                canGoNextSubj eats false
                doOnClick.invoke(tvTitle, context.getString(R.string.transition_string))
            }
        }
    }
}