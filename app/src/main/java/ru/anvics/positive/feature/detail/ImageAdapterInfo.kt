package ru.anvics.positive.feature.detail

import io.reactivex.subjects.BehaviorSubject
import ru.anvics.positive.R
import ru.anvics.positive.extentions.bs


// Crutch helper to transfer info between DetailActivity and ImageSliderActivity
// need to refactor both of activities
object ImageAdapterInfo {

    val posSubj: BehaviorSubject<Int> by lazy { bs<Int>() }

    val images = listOf(
            R.drawable.imgpsh_fullsize,
            R.drawable.imgpsh_fullsize_2,
            R.drawable.wallpaper)

    val currentPos: Int get() = posSubj.value ?: 0

    fun tag(position: Int): Int = images[position]
    fun tagForShadows(position: Int): String = position.toString()
}