package ru.anvics.positive.feature.common.view.view_pager


import android.view.GestureDetector
import android.view.MotionEvent

class XScrollDetector : GestureDetector.SimpleOnGestureListener() {
    override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
        return Math.abs(distanceX) > Math.abs(distanceY)
    }
}

class YScrollDetector : GestureDetector.SimpleOnGestureListener() {
    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        return Math.abs(distanceY * 0.5) > Math.abs(distanceX)
    }
}