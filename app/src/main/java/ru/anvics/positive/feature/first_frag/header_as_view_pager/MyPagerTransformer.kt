package ru.anvics.positive.feature.first_frag.header_as_view_pager

import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.first_header.view.*


class MyPagerTransformer : ViewPager.PageTransformer {
    private val TAG = "MyPagerTransformer"
    private val SCALE_FACTOR_SLIDE = 0.85f
    private val MIN_ALPHA_SLIDE = 0.35f

    private fun Float.setToViews() {
        views.forEach { it.translationX = this }
    }

    private var views: List<View> = listOf()
    private var pageWidth: Int = 0

    var isPageMovingLeft = false

    override fun transformPage(page: View, position: Float) {

        Log.i(TAG, "transformPage: isPageMovingLeft $isPageMovingLeft")

        setUpViews(page)
        pageWidth = page.width

        page.setLayerType(View.LAYER_TYPE_NONE, null)

        val ratioForLeftFrag = if (isPageMovingLeft) 1 else -1


        when {
            position < 0 && position > -0.5f -> {
                (-position * 0.5f * pageWidth * ratioForLeftFrag).setToViews()
            }
            position < -0.5f -> {
                ((1 + position) * 0.5f * pageWidth * ratioForLeftFrag).setToViews()
            }

            position >= 0 && position <= 0.5 -> {
                (-position * 0.5f * pageWidth * -ratioForLeftFrag).setToViews()
            }
            position > 0.5f -> {
                ((position - 1) * 0.5f * pageWidth * -ratioForLeftFrag).setToViews()
            }
        }

        //(cos((x + 1) * PI) / 2.0) + 0.5

        //mDimLabel.setTranslationX((float) (-(1 - position) * pageWidth));


/*        val alpha: Float
        val scale: Float
        val translationX: Float

        if (position < 0 && position > -1) {
            // this is the page to the left
            scale = Math.abs(Math.abs(position) - 1) * (1.0f - SCALE_FACTOR_SLIDE) + SCALE_FACTOR_SLIDE
            alpha = Math.max(MIN_ALPHA_SLIDE, 1 - Math.abs(position))
            val pageWidth = page.width
            val translateValue = position * -pageWidth
            if (translateValue > -pageWidth) {
                translationX = translateValue
            } else {
                translationX = 0f
            }
        } else {
            alpha = 1f
            scale = 1f
            translationX = 0f
        }

        page.alpha = alpha
        page.translationX = translationX
        page.scaleX = scale
        page.scaleY = scale*/
    }

    private fun setUpViews(page: View) {
        val view1 = page.llPlaceForText
        val view2 = page.first_black_rect
        val view3 = page.first_white_rect

        views = listOf(view1, view2, view3)
    }
}