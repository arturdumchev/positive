package ru.anvics.positive.feature.common.view.rv

import android.support.v4.util.SparseArrayCompat
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.ViewGroup
import ru.anvics.positive.feature.common.view.rv.IRvItem
import java.util.*


open class MyBaseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TAG = "MyBaseAdapter"

        fun fromAdapter(viewType: Int, delegate: IDelegateAdapter) = MyBaseAdapter().apply {
            delegateAdapters.put(viewType, delegate)
        }

        fun fromAdapters(vararg typeToAdapter: Pair<Int, IDelegateAdapter>) = MyBaseAdapter().apply {
            typeToAdapter.forEach { delegateAdapters.put(it.first, it.second) }
        }
    }

    protected val delegateItems: MutableList<IRvItem>
            by lazy(LazyThreadSafetyMode.NONE) { ArrayList<IRvItem>() }
    protected val delegateAdapters: SparseArrayCompat<IDelegateAdapter>
            by lazy(LazyThreadSafetyMode.NONE) { SparseArrayCompat<IDelegateAdapter>() }

    //—————————————————————————————————————————————————————————————————————— rv basics

    override fun getItemCount() = delegateItems.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position))
                .onBindViewHolder(holder, this.delegateItems[position])
    }

    override fun getItemViewType(position: Int): Int {
        return delegateItems[position].getViewType()
    }

    //—————————————————————————————————————————————————————————————————————— interface

    fun clearItems() {
        delegateItems.clear()
        notifyDataSetChanged()
    }

//    open fun fromObservable(observable: Observable<List<T>>) {
//        observable.subscribe { items ->
//            updateItems(items)
//        }
//    }

    open fun addItems(items: List<IRvItem>) {
        Log.i(TAG, "addItems: size == ${items.size}")
        val fromPos = delegateItems.size
        delegateItems.addAll(items)
        notifyItemRangeInserted(fromPos, items.size)
    }

    open fun getItems(): List<IRvItem> = delegateItems

    open fun updateItems(newItems: List<IRvItem>) {
        val diffCallback = BaseDiffUtils(delegateItems, newItems)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.delegateItems.clear()
        this.delegateItems.addAll(newItems)
        diffResult.dispatchUpdatesTo(this)
    }
}