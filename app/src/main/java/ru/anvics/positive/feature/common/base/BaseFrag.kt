package ru.anvics.positive.feature.common.base

import android.support.v4.app.Fragment
import android.view.ViewGroup
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import ru.anvics.positive.extentions.N
import ru.anvics.positive.extentions.addTo
import java.util.concurrent.TimeUnit


abstract class BaseFrag : Fragment() {
    private val TAG = "BaseFrag"
    protected val disposables by lazy(N) { CompositeDisposable() }

    abstract protected val activityRoot: ViewGroup

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun <T> Observable<T>.bindUi(action: (T) -> Unit) {
        observeOn(AndroidSchedulers.mainThread())
                .subscribe { t -> action.invoke(t) }
                .addTo(disposables)
    }

    fun doAfter(delay: Long, action: () -> Unit) {
        Completable.timer(delay, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { action.invoke() }
                .addTo(disposables)
    }
}