package ru.anvics.positive.feature.detail.image_slider

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.detail_image.view.*
import ru.anvics.positive.R
import ru.anvics.positive.extentions.inflate
import ru.anvics.positive.feature.common.view.view_pager.BasePagerAdapter
import ru.anvics.positive.feature.detail.ImageAdapterInfo


class ImageSliderPagerAdapter(
        imageAdapterInfo: ImageAdapterInfo,
        private val setUpTouchLogic: (View) -> Unit) : BasePagerAdapter<Int>(imageAdapterInfo.images) {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layout = container.inflate(R.layout.detail_image)
        (container as ViewPager).addView(layout)

        with(layout.detail_image_view) {
            val imgRes = items[position]
            setImageResource(imgRes)
            tag = imgRes
            transitionName = imgRes.toString()
        }

        setUpTouchLogic.invoke(layout)

        return layout
    }


    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }
}