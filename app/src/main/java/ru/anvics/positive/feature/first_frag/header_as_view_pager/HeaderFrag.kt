package ru.anvics.positive.feature.first_frag.header_as_view_pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.first_header.*
import ru.anvics.positive.R
import ru.anvics.positive.extentions.N
import ru.anvics.positive.extentions.inflate
import ru.anvics.positive.feature.common.base.BaseFrag


class HeaderFrag : BaseFrag() {
    companion object {
        private const val TAG = "HeaderFrag"
        private const val ARG_IMG_RES = "HeaderFrag"

        fun instance(imageRes: Int): HeaderFrag = HeaderFrag().apply {
            arguments = Bundle().apply { putInt(ARG_IMG_RES, imageRes) }
        }
    }

    interface ICallback {
        fun getImgYObservable(): Observable<Float>
        fun getTextYObservable(): Observable<Float>
    }

    private val imgRes: Int by lazy(N) { arguments.getInt(ARG_IMG_RES) }
    private var callback: ICallback? = null

    override val activityRoot: ViewGroup get() = first_fl_header

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?): View? {
        return v?.inflate(R.layout.first_header)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        first_parallax_img.background = resources.getDrawable(imgRes)

        callback = parentFragment as ICallback
        bindYObservables()

    }

    private fun bindYObservables() = with(callback!!) {

        getImgYObservable().zipWith(getTextYObservable(), BiFunction<Float, Float, Unit> {
            imageTop, textTop ->

            first_image_frame.setTopMar(imageTop)

            llPlaceForText.setTopMar(textTop)
            first_black_rect.setTopMar(textTop)

            first_white_rect.setTopMar(textTop - imageTop)

        }).bindUi { }
    }

    private fun View.setTopMar(mar: Float) {
        val lp = layoutParams as RelativeLayout.LayoutParams
        lp.topMargin = mar.toInt()
        layoutParams = lp
    }

    //——————————————————————————————————————————————————————————————————————


    override fun onDetach() {
        super.onDetach()
        callback = null
    }
}