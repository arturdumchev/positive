package ru.anvics.positive.feature.detail.image_slider

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import ru.anvics.positive.extentions.N
import ru.anvics.positive.extentions.screenWidth
import ru.anvics.positive.feature.common.view.view_pager.ViewPagerWithBlock
import ru.anvics.positive.feature.common.view.view_pager.YScrollDetector


class SlidesTouchHelper(private val bg: View,
                        private val commentViews: List<View>,
                        private val viewPager: ViewPagerWithBlock,
                        private val exitAction: () -> Unit) {

    companion object {
        private const val TAG = "SlidesTouchHelper"
    }

    private val context: Context get() = bg.context
    private val halfWidth: Float by lazy(N) { screenW / 2 }
    private val screenW: Float by lazy(N) { context.screenWidth().toFloat() }


    private var xCoordinate: Float = 0f
    private var yCoordinate: Float = 0f
    private var firstX: Float = 0f
    private var firstY: Float = 0f

    private var initialX: Float = 0f
    private var initialY: Float = 0f

    private var lastDist: Float = 0f


    fun setUpTouchView(view: View) {
        view.post {
            initialX = view.x
            initialY = view.y
        }
        view.setOnTouchListener { view, event ->

            when (event.action) {
                MotionEvent.ACTION_DOWN -> setUpStartCoordinates(event, view)
                MotionEvent.ACTION_MOVE -> onMove(event, view)
                MotionEvent.ACTION_UP -> onFingerUp(view)
                MotionEvent.ACTION_CANCEL -> onFingerUp(view)
            }
            true
        }
    }

    private var isLocked = false
    private var isActuallyDragging = false
    private val gestureDetector by lazy { GestureDetector(context, YScrollDetector()) }

    private fun onMove(event: MotionEvent, view: View) {

        if (!isActuallyDragging) {
            isLocked = !gestureDetector.onTouchEvent(event)
            if (!isLocked) {
                isActuallyDragging = true
                viewPager.isPagingEnabled = false
            }
        }
        if (isLocked) return

        lastDist = getDistViewDragged(halfWidth, view)
        val decreaseRatio: Float = getDecreaseRatio(lastDist)

        val xToMove = event.rawX + xCoordinate
        val yToMove = event.rawY + yCoordinate

        bg.animate().alpha(1 - decreaseRatio * 10f).setDuration(0).start()
        commentViews.forEach { it.animate().alpha(1 - decreaseRatio * 20f).setDuration(0).start() }

        view.animate()
                .scaleX(1 - decreaseRatio)
                .scaleY(1 - decreaseRatio)
                .x(xToMove)
                .y(yToMove)
                .setDuration(0)
                .start()
    }

    private fun onFingerUp(view: View) {
        isLocked = false
        isActuallyDragging = false
        viewPager.isPagingEnabled = true

        if (scrollTooFar(lastDist)) {
            exitAction.invoke()
        } else {
            bg.animate().alpha(0.95f).setDuration(300L).start()
            commentViews.forEach { it.animate().alpha(1f) }
            view.animate()
                    .x(initialX)
                    .y(initialY)
                    .scaleX(1f)
                    .scaleY(1f)
                    .setDuration(400)
                    .start()
        }
    }

    private fun setUpStartCoordinates(event: MotionEvent, view: View) {
        xCoordinate = view.x - event.rawX
        yCoordinate = view.y - event.rawY
        firstX = view.x
        firstY = view.y
    }

    //private fun scrollTooFar(lastDist: Float) = false
    private fun scrollTooFar(lastDist: Float) = lastDist > screenW / 5.5f

    private fun getDecreaseRatio(dist: Float) = (0.3f * dist) / screenW


    private fun getDistViewDragged(halfWidth: Float, view: View) = Math.abs(initialY + halfWidth - (view.y + halfWidth))
    /*Float {
val x = initialX + halfWidth
val y = initialY + halfWidth
val x1 = view.x + halfWidth
val y1 = view.y + halfWidth
return getDistBetweenTwoDots(x, y, x1, y1)
}
    private fun getDistBetweenTwoDots(x: Float, y: Float, x1: Float, y1: Float): Float {
        val dist1 = x - x1
        val dist2 = y - y1
        return Math.sqrt((dist1 * dist1 + dist2 * dist2).toDouble()).toFloat()
    }

    */
}







