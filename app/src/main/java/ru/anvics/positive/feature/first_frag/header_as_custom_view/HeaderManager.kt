package ru.anvics.positive.feature.first_frag.header_as_custom_view

import android.content.Context
import android.util.Log
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import android.widget.RelativeLayout
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.first_header.view.*
import kotlinx.android.synthetic.main.include_paging.view.*
import ru.anvics.positive.R
import ru.anvics.positive.extentions.*
import ru.anvics.positive.feature.first_frag.GestureListener
import java.util.*

/**
 * Temp solution; need to rewrite with view_pager

 * @see ru.anvics.positive.feature.first_frag.header_as_view_pager
 */
class HeaderManager(
        private val imgYObservable: Observable<Float>,
        private val textYObservable: Observable<Float>,
        private val rootRelativeLayout: RelativeLayout,
        private val pagingView: View,
        private val imgResList: List<Int>
) {
    companion object {
        private val TAG = HeaderManager::class.java.simpleName
    }

    private val context: Context get() = rootRelativeLayout.context
    private val width: Float by lazy { context.screenWidth().toFloat() }

    private val DURATION: Long = 450L
    private val DURATION_TEXT: Long = 670L
    private val interp: Interpolator by lazy { AccelerateDecelerateInterpolator() }
    private val placeForTextHeight by lazy(N) { context.getDimen(R.dimen.header_place_for_texth_height) }

    private val canGoLeft: Boolean get() = currentHeaderNum != 0
    private val canGoRight: Boolean get() = currentHeaderNum != headers.size - 1
    private var isAnimating: Boolean = false

    private lateinit var headers: List<View>

    private val disposables: CompositeDisposable by lazy() { CompositeDisposable() }


    fun init() {

        Log.i(TAG, "init: initing")
        val inflater = LayoutInflater.from(context)


        val views = ArrayList<View>()
        for (i in 0..imgResList.size - 1) {
            val view = inflater.inflate(R.layout.first_header, null)
            view.x = width * i
            view.first_parallax_img.setImageResource(imgResList[i])
            views.add(view)
            rootRelativeLayout.addView(view)
        }
        headers = views


//        val view1 = inflater.inflate(R.layout.first_header, null)
//        val view2 = inflater.inflate(R.layout.first_header, null)

//        view2.x = width
        //view2.first_parallax_img.background = context.resources.getDrawable(R.mipmap.imgpsh_fullsize_2)
//        view2.first_parallax_img.setImageResource(R.drawable.imgpsh_fullsize_2)
//        headers = listOf(view1, view2)

//        headers.forEach { rootRelativeLayout.addView(it) }

        headers.subscribeToObservables()
        setUp()
        pagingView.tvCountPage.text = headers.size.toString()
    }

    fun destroy() {
        rootRelativeLayout.clearFromChildren()
        disposables.clear()
    }
    private var currentHeaderNum = 0

    private fun onFling(left: Boolean) {

        pagingView.postDelayed({ pagingView.tvCurrentPage.text = (1 + currentHeaderNum).toString() }, DURATION)

        val indHelp = if (left) -1 else 1
        Log.i(TAG, "onFling: currentHeaderNum $currentHeaderNum")
        val newC = currentHeaderNum + indHelp
        Log.i(TAG, "onFling: about to set cur as ${newC}")
        headers[newC].bringToFront()
        headers.forEach { it.anim(left) }
    }


    private fun setUp() = with(context) {
        val gestureDetector = GestureDetector(this, GestureListener(width,
                {
                    if (!canGoLeft || isAnimating) return@GestureListener
                    currentHeaderNum--
                    onFling(false)
                },
                {
                    if (!canGoRight || isAnimating) return@GestureListener
                    currentHeaderNum++
                    onFling(true)
                }))
        rootRelativeLayout.setOnTouchListener { view, motionEvent ->
            gestureDetector.onTouchEvent(motionEvent)
        }
    }

    private fun List<View>.subscribeToObservables() {
        forEach { it.subscribeObservables() }
    }

    private fun View.subscribeObservables() {
        imgYObservable.zipWith(textYObservable, BiFunction<Float, Float, Unit> {
            imageTop, textTop ->
            first_image_frame.setTopMar(imageTop)

            llPlaceForText.setTopMar(textTop)
            first_black_rect.setTopMar(textTop)
            first_white_rect_frame.setTopMar(imageTop)
            first_white_rect.setTopMar(textTop - imageTop)

            pagingView.setTopMar(textTop + placeForTextHeight)

        }).subscribe().addTo(disposables)
    }

    private fun View.setTopMar(mar: Float) {
        val lp = layoutParams as RelativeLayout.LayoutParams
        lp.topMargin = mar.toInt()
        layoutParams = lp
    }

    private fun View.anim(left: Boolean) = with(context) {
        isAnimating = true
        val direction = ((if (left) -1 else 1) * screenWidth()).toFloat()
        first_image_frame.animate().xBy(direction).setInterpolator(interp).setDuration(DURATION)
        listOf(llPlaceForText, first_black_rect, first_white_rect).animateLabelViews(direction)
    }

    private fun List<View>.animateLabelViews(direction: Float) {
        forEach {
            it.animate().xBy(direction)
                    .setInterpolator(interp)
                    .setDuration(DURATION_TEXT)
                    .withEndAction { if (isAnimating) isAnimating = false }
        }
    }

}