package ru.anvics.positive.feature.detail

import android.support.v4.view.ViewPager
import android.view.View


class DetailPagerTransformer : ViewPager.PageTransformer {


    companion object{private const val TAG = "DetailPagerTransformer" }

    override fun transformPage(page: View, position: Float) {

        //Log.i(TAG, "transformPage: position == $position")

        var alpha = 1f - Math.abs(position)
        if(alpha < 0.4f) alpha = 0.4f
        page.alpha = alpha
        //page.detail_image_view.alpha = alpha
        //page.detail_image_view_shadow.alpha = alpha
        //page.detail_image_white_rect.alpha = 1f - alpha
    }
}