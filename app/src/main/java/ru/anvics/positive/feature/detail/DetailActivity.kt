package ru.anvics.positive.feature.detail

import android.content.Intent
import android.os.Bundle
import android.support.percent.PercentFrameLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.SharedElementCallback
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.PathInterpolator
import com.transitionseverywhere.ChangeBounds
import com.transitionseverywhere.Slide
import com.transitionseverywhere.TransitionManager
import com.transitionseverywhere.TransitionSet
import kotlinx.android.synthetic.main.detail_image.view.*
import kotlinx.android.synthetic.main.detail_info_activity.*
import kotlinx.android.synthetic.main.include_paging.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import ru.anvics.positive.MainActivity
import ru.anvics.positive.R
import ru.anvics.positive.extentions.*
import ru.anvics.positive.feature.common.base.BaseActivity
import ru.anvics.positive.feature.detail.image_slider.ImageSliderActivity


class DetailActivity : BaseActivity() {

    companion object {
        private const val TAG = "DetailActivity"

        var shouldScroll = false
        var lastPos = 0

        const val EXTRA_STARTING_POSITION = "extra_starting_item_position"

        const val LOADING_DELAY: Long = 2500
        const val SIDE_VIEWS_ALPHA: Float = 0.4f
        const val FADE_SIDE_VIEWS_TIME = 250L
        const val MONKEY_PROTECTION_TIME = 350L
    }

    private val info = ImageAdapterInfo
    private var mTmpReenterState: Bundle? = null
    private var isDetailActivityShown = true

    private val mCallback = object : SharedElementCallback() {
        override fun onMapSharedElements(names: MutableList<String>, sharedElements: MutableMap<String, View>) {
            if (mTmpReenterState != null) {
                val startingPosition = mTmpReenterState!!.getInt(EXTRA_STARTING_POSITION)
                val currentPosition = ImageAdapterInfo.currentPos

                Log.i(TAG, "onMapSharedElements: startingP $startingPosition, currentP $currentPosition")

                if (startingPosition != currentPosition) {

                    val res = ImageAdapterInfo.images[currentPosition]
                    val newSharedElement = detail_view_pager.findViewWithTag(info.tag(currentPosition))
                    val newTransitionName = res.toString()

                    if (newSharedElement != null) {
                        names.clear()
                        sharedElements.clear()
                        names.add(newTransitionName)
                        sharedElements.put(newTransitionName, newSharedElement)
                    }
                }
                mTmpReenterState = null
            }
        }
    }

    override val activityRoot: ViewGroup get() = detail_root

    private val progressViews by lazy { listOf(detail_progress, detail_progress_text) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_info_activity)
        setExitSharedElementCallback(mCallback)

        detail_monkey_protector.setOnClickListener { Log.i(TAG, "onAnyClickDelayedProtection: just a click") }

        translucentStatusBar()
        onPreparations()
        setUpViews()

        if (hasNavBar()) {
            detail_flex_layout.setPadding(0, 0, 0, (44 * density()).toInt())
        }
    }

    private fun setUpViews() {
        detail_back_img.setOnClickListener { onBackPressed() }
        detail_share_img.setOnClickListener { shareText("Безопасность домашних сетей.\n Все статьи в приложении https://ya.ru") }
        setUpViewPager()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        MainActivity.startMeLikeYouGoBack(this)
    }

    override fun onStop() {
        super.onStop()
        //overridePendingTransition(R.anim.stay, R.anim.exit)
    }

    override fun onActivityReenter(requestCode: Int, data: Intent) {
        super.onActivityReenter(requestCode, data)
        isDetailActivityShown = true
        Log.i(TAG, "onActivityReenter")

        //makeCenterViewShadowVisible(false)

        doAfter(350) {
            //            makeCenterViewShadowVisible(true)
            detail_white_rect.visibleNotGone(false)
            doAfter(50){fakeDragByPixel()}
        }

        detail_view_pager.findViewWithTag(info.images[info.currentPos]).visibleNotGone(true)

        //postponeEnterTransition()
        //detail_view_pager.onPreDraw { startPostponedEnterTransition() }
        mTmpReenterState = Bundle(data.extras)
    }

    private fun fakeDragByPixel() {
        detail_view_pager.beginFakeDrag()
        detail_view_pager.fakeDragBy(0.1f)
        detail_view_pager.endFakeDrag()
    }

    // to make all layout/detail_image invisible
    private fun makeCenterViewShadowVisible(visible: Boolean) {
        val viewParent = getViewPagerParentViewByPos(info.currentPos)
        viewParent?.detail_image_view_shadow?.visibleNotGone(visible)
    }

/*    private fun makeCenterViewWhiteRectVisible(visible: Boolean) {
        val viewParent = getViewPagerParentViewByPos(info.currentPos)
        viewParent?.detail_image_white_rect?.visibleNotInvisible(visible)
    }*/

    private fun setUpViewPager() {
        info.posSubj eats 0

        val adapter = DetailImageAdapter(ImageAdapterInfo, { v, i -> navigateToDetail(i, v) })
        tvCountPage.text = (adapter.count).toString()

        setUpViewPagerSettings(adapter)

        setUpViewPagerListeners()
        hideAllShadowViewPagerImages()
    }


    private fun setUpViewPagerSettings(adapter: DetailImageAdapter) {
        detail_view_pager.adapter = adapter
        detail_view_pager.offscreenPageLimit = adapter.count
        detail_view_pager.offscreenPageLimit = 3
        detail_view_pager.clipChildren = false
        detail_view_pager.setScrollDuration(320)
        detail_view_pager.setPageTransformer(true, DetailPagerTransformer())
        //detail_view_pager.setPageTransformer(true, null)
    }

    private fun hideAllShadowViewPagerImages(shouldHide: Boolean = true) {
        detail_view_pager.post {
            info.images.indices.forEach { i -> getViewPagerShadowByPos(i).visibleNotGone(!shouldHide) }
        }
    }

    private fun setUpViewPagerListeners() {
        detail_view_pager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                tvCurrentPage.text = (position + 1).toString()

                if (info.currentPos != position) info.posSubj eats position
            }
        })

        info.posSubj.bindUi { pos ->
            if (pos != detail_view_pager.currentItem) {
                detail_view_pager.setCurrentItem(pos, true)
            }
        }

        detail_view_pager.post {
            detail_vp_left_touch_area.onAreaClicked(false)
            detail_vp_right_touch_area.onAreaClicked(true)
        }
    }

    private fun updateAlphaOnScrolled(pos: Int, posOffset: Float) {

        Log.i(TAG, "updateAlphaOnScrolled: pos $pos, offset $posOffset")
        var alpha = 1f - Math.abs(posOffset)
        if (alpha < 0.4f) alpha = 0.4f
        getViewPagerParentViewByPos(pos)?.alpha = alpha
        //if (pos + 1 < info.images.size) getViewPagerParentViewByPos(pos + 1)?.alpha = alpha
    }

    private inner class TapGestureListener(val doOnClick: () -> Unit) : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent?): Boolean = true
        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            Log.i(TAG, "onSingleTapUp")
            doOnClick.invoke()
            return true
        }

        override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
            return false
        }
        override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean = false
    }

    private fun View.onAreaClicked(isIncrement: Boolean) {


        val gesturer = GestureDetector(context, TapGestureListener({


            val pos = if (isIncrement) 1 else -1
            val pager = this@DetailActivity.detail_view_pager
            val currentPos = pager.currentItem + pos
            if (currentPos >= 0 && currentPos != info.images.size) {
                val view = pager.findViewWithTag(info.tag(currentPos))
                navigateToDetail(currentPos, view)
                onAnyClickDelayedProtection()
            }
        }))

        setOnTouchListener { view, motionEvent ->
            detail_view_pager.onTouchEvent(motionEvent)
            gesturer.onTouchEvent(motionEvent)
        }

        /*setOnClickListener {
            val pos = if (isIncrement) 1 else -1
            val pager = this@DetailActivity.detail_view_pager
            val currentPos = pager.currentItem + pos
            if (currentPos < 0 || currentPos == info.images.size) return@setOnClickListener

            val view = pager.findViewWithTag(info.tag(currentPos))
            navigateToDetail(currentPos, view)
            onAnyClickDelayedProtection()
        }*/
    }

    private fun swipe(isIncrement: Boolean) {
        if (isIncrement) detail_view_pager.currentItem++
        else detail_view_pager.currentItem--
        onAnyClickDelayedProtection()
    }

    private fun onAnyClickDelayedProtection() {
        Log.e(TAG, "onAnyClickDelayedProtection: setting click listener")
        detail_monkey_protector.visibleNotGone(true)
        doAfter(MONKEY_PROTECTION_TIME) { detail_monkey_protector.visibleNotGone(false) }
    }

    private fun setAlphaOnPosition(pos: Int) {

        try {
            val leftView: View? = if (pos > 0) getViewPagerParentViewByPos(pos - 1) else null
            val rightView: View? = if (pos < info.images.size - 1) getViewPagerParentViewByPos(pos + 1) else null
            val centerView: View? = getViewPagerParentViewByPos(pos)

            leftView?.animate()?.alpha(SIDE_VIEWS_ALPHA)?.duration = FADE_SIDE_VIEWS_TIME
            rightView?.animate()?.alpha(SIDE_VIEWS_ALPHA)?.duration = FADE_SIDE_VIEWS_TIME
            centerView?.alpha = 1f

        } catch (e: NullPointerException) {
            Log.e(TAG, "setAlphaOnPosition: we already exited")
        }
    }

    private fun getViewPagerShadowByPos(pos: Int): View = detail_view_pager.findViewWithTag(info.tagForShadows(pos))

    private fun getViewPagerParentViewByPos(pos: Int): View? = getPagerViewByTag(info.tag(pos))

    private fun getPagerViewByTag(res: Int): View? {
        val currentView: View? = detail_view_pager.findViewWithTag(res)
        val viewParent: View? = currentView?.parent as View?
        return viewParent
    }

    private fun navigateToDetail(i: Int, v: View) {

        hideAllShadowViewPagerImages()
        isDetailActivityShown = false
        doAfter(450) {
            if (isDetailActivityShown) return@doAfter
            detail_white_rect.visibleNotGone(true)
            getViewPagerShadowByPos(i).visibleNotGone(true)
        }

        v.navigateToDetail(i)
    }

    private fun View.navigateToDetail(pos: Int) {
        Log.i(TAG, "navigateToDetail: pos == $pos")

        val intent = Intent(this@DetailActivity, ImageSliderActivity::class.java)
        intent.putExtra(EXTRA_STARTING_POSITION, pos)
        val options: ActivityOptionsCompat = ActivityOptionsCompat
                .makeSceneTransitionAnimation(this@DetailActivity, this, this.transitionName)
        ActivityCompat.startActivity(this@DetailActivity, intent, options.toBundle())
    }

    private fun onPreparations() {
        OverScrollDecoratorHelper.setUpOverScroll(detail_scroll)
        ViewCompat.setTransitionName(detail_title, getString(R.string.transition_string))
        allViewsVisible(false)
        detail_title.visibleNotGone(true)
        showProgressViews()

        doAfter(LOADING_DELAY) {
            detail_monkey_protector.visibleNotGone(false)
            TransitionManager.beginDelayedTransition(detail_root)
            allViewsVisible(true)
            onProgressEnd()
        }
    }

    private fun showProgressViews() {
        doAfter(400) { progressViews.forEach { it.visibleNotGone(true) } }
    }

    private fun onProgressEnd() {
        progressViews.forEach { it.visibleNotGone(false) }
        doAfter(90) {
            TransitionManager.beginDelayedTransition(detail_root, TransitionSet()
                    .addTransition(Slide())
                    .addTransition(ChangeBounds())
                    .setDuration(600)
                    .setInterpolator(PathInterpolator(0.25f, 0.1f, 0.25f, 1f)))
            val lp = detail_scroll.layoutParams as PercentFrameLayout.LayoutParams
            lp.percentLayoutInfo.topMarginPercent = 0f
            detail_scroll.layoutParams = lp
        }
    }

    private fun allViewsVisible(visible: Boolean) {
        detail_flex_layout.onEachChild<View> { view -> view.visibleNotGone(visible) }
    }
}