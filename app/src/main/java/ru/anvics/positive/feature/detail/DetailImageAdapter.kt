package ru.anvics.positive.feature.detail

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.detail_image.view.*
import ru.anvics.positive.R
import ru.anvics.positive.extentions.inflate
import ru.anvics.positive.feature.common.view.view_pager.BasePagerAdapter


open class DetailImageAdapter(val imageAdapterInfo: ImageAdapterInfo,
                              val doOnClick: (View, Int) -> Unit) : BasePagerAdapter<Int>(imageAdapterInfo.images) {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layout = container.inflate(R.layout.detail_image)
        (container as ViewPager).addView(layout)

        val imgRes = items[position]
        with(layout.detail_image_view) {
            setImageResource(imgRes)
            tag = imgRes
            transitionName = imgRes.toString()

            setOnClickListener { doOnClick.invoke(detail_image_view, position) }
        }

        with(layout.detail_image_view_shadow) {
            //visibleNotGone(true)
            tag = imageAdapterInfo.tagForShadows(position)
            setImageResource(imgRes)
        }

        return layout
    }

    override fun getItemPosition(`object`: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }
}
