package ru.anvics.positive.feature.first_frag

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.animation.DecelerateInterpolator
import io.reactivex.subjects.BehaviorSubject
import ru.anvics.positive.R
import ru.anvics.positive.extentions.N
import ru.anvics.positive.extentions.eats
import ru.anvics.positive.extentions.getDimen
import ru.anvics.positive.extentions.visibleNotGone

class MyScrollListener(
        val header: View,
        val footer: View,
        val pagingView: View,
        footerBottomView: View,
        val headerTextY: Float,
        val imageYSubj: BehaviorSubject<Float>,
        val textYSubj: BehaviorSubject<Float>) : RecyclerView.OnScrollListener() {

    companion object {
        private const val TAG = "MyScrollListener"
        private const val FOOTER_SPEED = 320L
    }

    private var yRvSum = 0
    private var shouldHideHeader: Boolean = false
    private var isHeaderShown: Boolean = false
    private val headerHeight: Float by lazy(N) { header.context.getDimen(R.dimen.header_height).toFloat() }

    private var yFooterSum = 0
    private var isUserDrags: Boolean = true
    private var isFooterShown: Boolean = true
    private var footerHeight: Float = footer.height()
    private var footerY: Float = footer.y

    private var isSettling = true
    private var isShowingFreeFooterAnim = false
    private var justAutoScrolled = false

    init {
        footer.post { footerHeight = footer.height() }
        footer.post { footerY = footer.y }

        footerBottomView.setOnClickListener { if (!isFooterShown) hideFooter(false) }
    }

    private fun View.height() = height.toFloat()

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        calcAttrForHeader(recyclerView)
        updateHeaderOnScrolled()
        showIfBottom(recyclerView)

        animateFooterOnScroll(dy)
    }

    private fun animateFooterOnScroll(dy: Int) {

        if (isUserDrags) {
            //if (!isIdle) {
            yFooterSum += (dy * 0.6).toInt()
            protectFromOverSlideFooter()
            footer.animate().y(footerY + yFooterSum).setDuration(0).start()
        } else if (!isShowingFreeFooterAnim) {
            hideOrShowFooter()
        }
    }

    private fun protectFromOverSlideFooter() {
        if (yFooterSum > footerHeight) yFooterSum = footerHeight.toInt()
        else if (yFooterSum < 0) yFooterSum = 0
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        calcAttrForHeader(recyclerView)

        updateHeaderOnScrollStateChanged(newState, recyclerView)
        showIfBottom(recyclerView)
    }

    private fun showIfBottom(recyclerView: RecyclerView) {
        //val lm = (recyclerView.layoutManager as LinearLayoutManager)
        //val isLast = lm.findLastCompletelyVisibleItemPosition() == FirstFrag.MAX_ROWS - 1

        val canScrollVertical = recyclerView.canScrollVertically(1)
        //Log.i(TAG, "showIfBottom: canScrollVertical == $canScrollVertical")
        if (!canScrollVertical) hideFooter(false)
    }

    private fun calcAttrForHeader(recyclerView: RecyclerView) {
        yRvSum = recyclerView.computeVerticalScrollOffset() //how long I have scrolled from the beginning
        isHeaderShown = headerHeight * 0.85 > yRvSum
    }

    private fun updateHeaderOnScrolled() {
        header.visibleNotGone(isHeaderShown)

        if (isHeaderShown) {
            imageYSubj eats -yRvSum * 1.2f
            textYSubj eats headerTextY - yRvSum * 1.35f
        }
    }

    private var lastState = 0

    private fun updateHeaderOnScrollStateChanged(newState: Int, recyclerView: RecyclerView) {

        isUserDrags = RecyclerView.SCROLL_STATE_DRAGGING == newState
        isSettling = RecyclerView.SCROLL_STATE_SETTLING == newState

        isShowingFreeFooterAnim = !isUserDrags && !isSettling

        pagingView.visibleNotGone(isHeaderShown)

        when (newState) {
            RecyclerView.SCROLL_STATE_DRAGGING -> {
                justAutoScrolled = false
            }
            RecyclerView.SCROLL_STATE_IDLE -> {
                if (isHeaderShown) autoScroll(recyclerView)
                else if (lastState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    hideOrShowFooter()
                    recyclerView.postDelayed({if(footer.y != footerY) hideOrShowFooter()}, 300)
                }
            }
            RecyclerView.SCROLL_STATE_SETTLING -> {
                justAutoScrolled = false
                if (isHeaderShown) {
                    recyclerView.postDelayed({ if (isHeaderShown) autoScroll(recyclerView) }, 180)
                } else hideOrShowFooter()
            }
        }
        lastState = newState
    }

    private fun autoScroll(recyclerView: RecyclerView) {

        if (justAutoScrolled) return

        justAutoScrolled = true

        shouldHideHeader = (headerHeight - yRvSum) < headerHeight / 2
        if (shouldHideHeader) recyclerView.smoothScrollToPosition(2)
        else recyclerView.smoothScrollToPosition(0)

        if (!shouldHideHeader) hideFooter(false)
    }


    private fun hideOrShowFooter() {
        hideFooter(isFooterCloserToBottom())
    }

    private fun hideFooter(shouldHide: Boolean) {
        Log.i(TAG, "hideFooter $shouldHide")
        isFooterShown = !shouldHide
        isShowingFreeFooterAnim = true
        footer.animate()
                .y(if (shouldHide) footerY + footerHeight else footerY)
                .setDuration(FOOTER_SPEED)
                .setInterpolator(DecelerateInterpolator())
                .withEndAction {
                    isShowingFreeFooterAnim = false

                    if (shouldHide) yFooterSum = footerHeight.toInt()
                    else yFooterSum = 0
                }
    }

    private fun isFooterCloserToBottom() = footer.y > footerY + footerHeight / 2
}