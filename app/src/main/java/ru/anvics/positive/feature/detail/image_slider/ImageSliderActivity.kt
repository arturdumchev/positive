package ru.anvics.positive.feature.detail.image_slider

import android.app.Activity
import android.app.SharedElementCallback
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import kotlinx.android.synthetic.main.detail_image.view.*
import kotlinx.android.synthetic.main.image_slider_activity.*
import kotlinx.android.synthetic.main.include_paging_white.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import ru.anvics.positive.R
import ru.anvics.positive.extentions.N
import ru.anvics.positive.extentions.density
import ru.anvics.positive.extentions.eats
import ru.anvics.positive.extentions.onPreDraw
import ru.anvics.positive.feature.common.base.BaseActivity
import ru.anvics.positive.feature.common.view.view_pager.CustomScroller
import ru.anvics.positive.feature.detail.DetailActivity
import ru.anvics.positive.feature.detail.DetailActivity.Companion.EXTRA_STARTING_POSITION
import ru.anvics.positive.feature.detail.ImageAdapterInfo


class ImageSliderActivity : BaseActivity() {

    companion object {

        private const val TAG = "ImageSlider"
        private val STATE_CURRENT_PAGE_POSITION = "state_current_page_position"
    }

    private val mStartingPosition: Int by lazy { intent.getIntExtra(DetailActivity.EXTRA_STARTING_POSITION, 0) }
    private var mIsReturning: Boolean = false

    override val activityRoot: ViewGroup get() = image_slider_activity

    private val mCallback = object : SharedElementCallback() {
        override fun onMapSharedElements(names: MutableList<String>, sharedElements: MutableMap<String, View>) {
            if (mIsReturning) {
                val currentPos = ImageAdapterInfo.currentPos
                val res = ImageAdapterInfo.images[currentPos]
                val sharedElement = image_slider_view_pager.findViewWithTag(res)
                if (mStartingPosition != currentPos) {
                    // If the user has swiped to a different ViewPager page, then we need to
                    // remove the old shared element and replace it with the new shared element
                    // that should be transitioned instead.
                    names.clear()
                    names.add(sharedElement.transitionName)
                    sharedElements.clear()
                    sharedElements.put(sharedElement.transitionName, sharedElement)
                }
            }
        }
    }

    private val SlidesTouchHelper: SlidesTouchHelper by lazy(N) {
        SlidesTouchHelper(image_bg_view,
                listOf(image_paging, image_comment_tv, image_exit_but),
                image_slider_view_pager) { onBackPressed() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.image_slider_activity)
        //translucentStatusBar()
        setEnterSharedElementCallback(mCallback)
        postponeEnterTransition()

        setUpViewPager()
        image_exit_but.setOnClickListener { onBackPressed() }
        tvCountPage_white.text = ImageAdapterInfo.images.size.toString()
    }

    private fun setUpViewPager() {

        val customScroller = CustomScroller(this, AccelerateInterpolator())
        val mScroller = ViewPager::class.java.getDeclaredField("mScroller")
        mScroller.isAccessible = true
        mScroller.set(image_slider_view_pager, customScroller)

        val adapter = ImageSliderPagerAdapter(ImageAdapterInfo, { it.detail_image_view.setUpAggressiveTouchPolicy() })
        image_slider_view_pager.adapter = adapter

        image_slider_view_pager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                ImageAdapterInfo.posSubj eats position
                tvCurrentPage_white.text = (position + 1).toString()
            }
        })

        image_slider_view_pager.currentItem = mStartingPosition
        image_slider_view_pager.postDelayed({
            ImageAdapterInfo.posSubj eats mStartingPosition
        }, 50)

        image_slider_view_pager.onPreDraw { startPostponedEnterTransition() }
        OverScrollDecoratorHelper.setUpOverScroll(image_slider_view_pager)

        image_slider_view_pager.pageMargin = (9 * density()).toInt()
    }

    private fun View.setUpAggressiveTouchPolicy() {
        SlidesTouchHelper.setUpTouchView(this)
    }

    override fun finishAfterTransition() {
        mIsReturning = true
        val data = Intent().apply { putExtra(EXTRA_STARTING_POSITION, mStartingPosition) }
        setResult(Activity.RESULT_OK, data)
        super.finishAfterTransition()
    }
}
