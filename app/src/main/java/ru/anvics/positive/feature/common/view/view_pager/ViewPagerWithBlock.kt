package ru.anvics.positive.feature.common.view.view_pager

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.Interpolator
import android.widget.Scroller


class ViewPagerWithBlock : ViewPager {
    companion object {
        private const val SPEED = 250
        private val TAG = ViewPagerWithBlock::class.java.simpleName
    }

    private var mScroller: FixedSpeedScroller? = null
    var isPagingEnabled = true

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    /*
     * Override the Scroller instance with our own class so we can change the
     * duration
     */
    private fun init() {

        val viewpager = ViewPager::class.java
        val scroller = viewpager.getDeclaredField("mScroller")
        //scroller.isAccessible = true
        //mScroller = FixedSpeedScroller(context, AccelerateInterpolator())
        //scroller.set(this, mScroller)
    }

    /*
     * Set the factor by which the duration will change
     */
    fun setScrollDuration(duration: Int) {
        mScroller!!.setScrollDuration(duration)
    }

    private inner class FixedSpeedScroller(context: Context, interpolator: Interpolator) : Scroller(context, interpolator) {

        private var mDuration = SPEED

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration)
        }

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration)
        }

        fun setScrollDuration(duration: Int) {
            mDuration = duration
        }
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return isPagingEnabled && super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return isPagingEnabled && super.onTouchEvent(event)
    }
}