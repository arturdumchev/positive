package ru.anvics.positive.feature.first_frag.header_as_view_pager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import ru.anvics.positive.R


class HeaderAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> HeaderFrag.instance(R.drawable.imgpsh_fullsize)
        1 -> HeaderFrag.instance(R.drawable.imgpsh_fullsize_2)
        else -> TODO("Only 2 frags in first ver")
    }

    override fun getCount(): Int = 2
}