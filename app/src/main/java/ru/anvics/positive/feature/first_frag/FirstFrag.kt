package ru.anvics.positive.feature.first_frag

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.first_feed_frag.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import ru.anvics.positive.MainActivity
import ru.anvics.positive.R
import ru.anvics.positive.extentions.*
import ru.anvics.positive.feature.common.base.BaseFrag
import ru.anvics.positive.feature.common.view.rv.ITEM_NEWS_ID
import ru.anvics.positive.feature.common.view.rv.MyBaseAdapter
import ru.anvics.positive.feature.first_frag.header_as_custom_view.HeaderManager
import ru.anvics.positive.feature.first_frag.header_as_view_pager.HeaderFrag
import java.util.*


class FirstFrag : BaseFrag(), HeaderFrag.ICallback {
    companion object {
        private val TAG = FirstFrag::class.java.simpleName
        const val MAX_ROWS = 9
    }

    override val activityRoot: ViewGroup get() = first_frag_root

    private val imageYSubj: BehaviorSubject<Float> by lazy { bs<Float>() }
    private val textYSubj: BehaviorSubject<Float> by lazy { bs<Float>() }

    private val canGoNextSubj: BehaviorSubject<Boolean> = bs()

    private val lm by lazy(N) { MyCustomLayoutManager(context) }
    private val rvAdapter by lazy {
        MyBaseAdapter.fromAdapter(ITEM_NEWS_ID,
                SimpleViewHolder({ view, name -> navigateToDetail(view, name) }, canGoNextSubj))
    }
    private lateinit var headerManager: HeaderManager

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?)
            = v?.inflate(R.layout.first_feed_frag)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRv()
        setUpScrollListeners()
        setUpHeader()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        headerManager.destroy()
    }

    override fun onResume() {
        super.onResume()

        canGoNextSubj eats true

        view!!.isFocusableInTouchMode = true
        view!!.requestFocus()
        view!!.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                if (childFragmentManager.backStackEntryCount > 0) childFragmentManager.popBackStack()
                else activity.onBackPressed()
                return@OnKeyListener true
            }

            false
        })
    }


    override fun onPause() {
        super.onPause()
//        DetailActivity.shouldScroll = true
//        DetailActivity.lastPos = lm.findLastCompletelyVisibleItemPosition()
    }

    private fun setUpRv() {

//        val layoutMan = SnappyLinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//        layoutMan.setSnapInterpolator(DecelerateInterpolator())
//        layoutMan.setSnapType(SnapType.START)

        first_rv.apply {
            //layoutManager = layoutMan
            layoutManager = lm
            if (adapter == null) adapter = rvAdapter.apply { addItems(getData()) }
            OverScrollDecoratorHelper.setUpOverScroll(this, OverScrollDecoratorHelper.ORIENTATION_VERTICAL)
        }

        //if (DetailActivity.shouldScroll) scrollToPos(DetailActivity.lastPos)
    }

    fun scrollToPos(pos: Int) {
        if (pos == 0) return
        first_rv.scrollToPosition(pos)
        first_header_layout.visibleNotGone(false)
        first_paging.visibleNotGone(false)
    }

    private fun navigateToDetail(view: View, transName: String) {
        view.transitionName = transName
        ViewCompat.setTransitionName(view, transName)
        (activity as MainActivity).navigate(view, transName)
    }

    private fun setUpHeader() {
        headerManager = HeaderManager(imageYSubj, textYSubj,
                first_header_layout,
                first_paging,
                listOf(R.drawable.imgpsh_fullsize, R.drawable.imgpsh_fullsize_2))
        headerManager.init()
    }

    private fun setUpViewPager() {

/*        private val headerAdapter by lazy { HeaderAdapter(childFragmentManager) }

        first_view_pager.adapter = headerAdapter
        val myPagerTransformer = MyPagerTransformer()
        first_view_pager.setPageTransformer(true, myPagerTransformer)
        first_view_pager.addOnPageChangeListener(MyPagerChangeListener(myPagerTransformer))
        first_view_pager.offscreenPageLimit = 2

        first_view_pager.clipChildren = false
        first_view_pager.clipToPadding = false


        first_view_pager.setPagingEnabled(false)
        first_view_pager.setOnTouchListener { view, motionEvent ->
            gestureDetector.onTouchEvent(motionEvent)
        }*/
    }

    private fun setUpScrollListeners() {
        first_header_layout.post {

            Log.i(TAG, "setUpScrollListeners: first_view_pager.post ")

            val height = first_header_layout.height
            val textY = context.getDimen(R.dimen.header_text_top_mar).toFloat()

            val scrollListener = MyScrollListener(
                    first_header_layout,
                    (activity as MainActivity).getTabView(),
                    first_paging,
                    first_bottom_area,
                    textY,
                    imageYSubj,
                    textYSubj)

            first_rv.addOnScrollListener(scrollListener)
        }
    }

    private fun getData(): List<ItemNews> {
        return ArrayList<ItemNews>().apply {
            for (i in 0..MAX_ROWS - 1) {
                add(ItemNews("6 сен",
                        getString(R.string.value_title_item_feed),
                        getString(R.string.value_message_item_feed)))
            }
        }
    }

    //—————————————————————————————————————————————————————————————————————— HeaderFrag.Callback
    override fun getImgYObservable(): Observable<Float> = imageYSubj

    override fun getTextYObservable(): Observable<Float> = textYSubj
}