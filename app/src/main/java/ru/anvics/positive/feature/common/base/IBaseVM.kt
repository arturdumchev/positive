package ru.anvics.positive.feature.common.base

import io.reactivex.Observable


interface IBaseVM {

    fun getProgress(): Observable<Boolean>

    fun end()
}