package ru.anvics.positive.feature.first_frag;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MyCustomLayoutManager extends LinearLayoutManager {
    private static final float MILLISECONDS_PER_INCH = 300f;
    private Context mContext;

    public MyCustomLayoutManager(Context context) {
        super(context);
        mContext = context;
    }


/*    float MANUAL_SCROLL_SLOW_RATIO = 1f

    @Override public int scrollVerticallyBy(int delta, RecyclerView.Recycler recycler, RecyclerView.State state) {
        // write your limiting logic here to prevent the delta from exceeding the limits of your list.

        int prevDelta = delta;
        if (recycler.getScrollState() == SCROLL_STATE_DRAGGING)
            delta = (int)(delta > 0 ? Math.max(delta * MANUAL_SCROLL_SLOW_RATIO, 1) : Math.min(delta * MANUAL_SCROLL_SLOW_RATIO, -1));

        // MANUAL_SCROLL_SLOW_RATIO is between 0 (no manual scrolling) to 1 (normal speed) or more (faster speed).
        // write your scrolling logic code here whereby you move each view by the given delta

        if (getScrollState() == SCROLL_STATE_DRAGGING)
            delta = prevDelta;

        return delta;
    }*/

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView,
                                       RecyclerView.State state, final int position) {

/*        LinearSmoothScroller smoothScroller =
                new LinearSmoothScroller(mContext) {


                    @Override protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
                    }
                };

        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);*/


/*        SnappySmoothScroller scroller = new SnappySmoothScroller.Builder()
                .setPosition(position)
                .setScrollVectorDetector(new LinearLayoutScrollVectorDetector(this))
                .build(recyclerView.getContext());


        //android:paddingTop="@dimen/first_rv_top_pad"
        scroller.setSnapInterpolator(new DecelerateInterpolator());
        scroller.setSnapType(SnapType.START);
        //scroller.setSnapPaddingStart(ContextExtentionsKt.getDimen(mContext, R.dimen.first_rv_top_pad));*/


        MyLinearSmoothScroller scroller = new MyLinearSmoothScroller(mContext);
        scroller.setTargetPosition(position);

        startSmoothScroll(scroller);

    }
}
