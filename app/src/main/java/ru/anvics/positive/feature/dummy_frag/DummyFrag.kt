package ru.anvics.positive.feature.dummy_frag

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dummy_frag.*
import ru.anvics.positive.R
import ru.anvics.positive.extentions.N
import ru.anvics.positive.extentions.inflate
import ru.anvics.positive.feature.common.base.BaseFrag


class DummyFrag : BaseFrag() {
    companion object {
        private val TAG = DummyFrag::class.java.simpleName
        private val ARG_IMG_RES = "ARG_IMG_RES"

        fun instance(res: Int) = DummyFrag().apply {
            arguments = Bundle().apply { putInt(ARG_IMG_RES, res) }
        }
    }

    private val imgRes: Int by lazy(N) { arguments.getInt(ARG_IMG_RES) }

    override val activityRoot: ViewGroup get() = dummy_root

    override fun onCreateView(i: LayoutInflater?, v: ViewGroup?, s: Bundle?)
            = v?.inflate(R.layout.dummy_frag)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dummy_image.setImageResource(imgRes)
    }
}