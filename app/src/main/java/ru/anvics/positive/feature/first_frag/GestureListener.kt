package ru.anvics.positive.feature.first_frag

import android.view.GestureDetector
import android.view.MotionEvent

open class GestureListener(
        private val screenWidth: Float,
        private val onSwipeRight: () -> Unit,
        private val onSwipeLeft: () -> Unit) : GestureDetector.SimpleOnGestureListener() {

    companion object {
        private val SWIPE_THRESHOLD = 100
        private val SWIPE_VELOCITY_THRESHOLD = 100
        private val TAG = GestureListener::class.java.simpleName
    }

    override fun onDown(e: MotionEvent): Boolean {

        if (e.x <= screenWidth / 3) {
            onSwipeRight.invoke()
        } else if (e.x >= 2 * screenWidth / 3) {
            onSwipeLeft.invoke()
        }

        return true
    }

    override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {

        val diffY = e2.y - e1.y
        val diffX = e2.x - e1.x
        if (Math.abs(diffX) > Math.abs(diffY)) {
            if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (diffX > 0) {
                    onSwipeRight.invoke()
                } else {
                    onSwipeLeft.invoke()
                }
            }
        }

        return true
    }
}

