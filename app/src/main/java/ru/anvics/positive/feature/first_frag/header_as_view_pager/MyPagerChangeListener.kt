package ru.anvics.positive.feature.first_frag.header_as_view_pager

import android.support.v4.view.ViewPager


class MyPagerChangeListener(val pagerTransformer: MyPagerTransformer) : ViewPager.OnPageChangeListener {
    companion object {
        private val TAG = MyPagerChangeListener::class.java.simpleName
    }

    private var sum: Float = 0f

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        pagerTransformer.isPageMovingLeft = position + positionOffset > sum
        sum = position + positionOffset
    }

    override fun onPageSelected(state: Int) {
    }
}