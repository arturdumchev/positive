package ru.anvics.positive.feature.common.base

import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import ru.anvics.positive.extentions.N
import ru.anvics.positive.extentions.addTo
import java.util.concurrent.TimeUnit




abstract class BaseActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "BaseActivity"
        val IMAGE_TRANSITION_NAME = "activity_image_transition"
    }


    abstract protected val activityRoot: ViewGroup

    protected val disposables by lazy(N) { CompositeDisposable() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    //—————————————————————————————————————————————————————————————————————— rx helpers
    fun <T> Observable<T>.bindUi(action: (T) -> Unit) {
        compose(onUiCachedScheduler())
                .subscribe { t -> action.invoke(t) }
                .addTo(disposables)
    }

    //——————————————————————————————————————————————————————————————————————

    fun hideStatusBar(){
        val decorView = window.decorView
        val uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN
        decorView.systemUiVisibility = uiOptions
    }

    protected fun translucentStatusBar() {
        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        window.statusBarColor = Color.TRANSPARENT
    }

    fun doAfter(delay: Long, action: () -> Unit) {
        Completable.timer(delay, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { action.invoke() }
                .addTo(disposables)
    }

    fun Class<*>.goTo() = with(this@BaseActivity) {
        startActivity(Intent(this, this@goTo))
    }

    private val schedulersTransformer = ObservableTransformer<Any, Any> { upstream ->
        upstream.observeOn(AndroidSchedulers.mainThread())
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> onUiCachedScheduler() = schedulersTransformer as ObservableTransformer<T, T>

    fun hasNavBar(): Boolean {
        val realSize = Point()
        val screenSize = Point()
        var hasNavBar = false
        val metrics = DisplayMetrics()
        this.getWindowManager().getDefaultDisplay().getRealMetrics(metrics)
        realSize.x = metrics.widthPixels
        realSize.y = metrics.heightPixels
        getWindowManager().getDefaultDisplay().getSize(screenSize)
        if (realSize.y !== screenSize.y) {
            val difference = realSize.y - screenSize.y
            var navBarHeight = 0
            val resources = resources
            val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
            if (resourceId > 0) {
                navBarHeight = resources.getDimensionPixelSize(resourceId)
            }
            if (navBarHeight != 0) {
                if (difference == navBarHeight) {
                    hasNavBar = true
                }
            }

        }
        return hasNavBar

    }
}