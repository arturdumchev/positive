package ru.anvics.positive.feature.common.base

import android.util.Log
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import ru.anvics.positive.extentions.addTo
import ru.anvics.positive.extentions.bs
import ru.anvics.positive.extentions.eats


abstract class BaseVM : IBaseVM {
    companion object {
        private val TAG = BaseVM::class.java.simpleName
    }

    protected val disposables by lazy(ru.anvics.positive.extentions.N) { CompositeDisposable() }

    protected val progressSubj: BehaviorSubject<Boolean> = bs()

    override fun getProgress(): Observable<Boolean> = progressSubj
    override fun end() {
        disposables.clear()
    }

    //—————————————————————————————————————————————————————————————————————— progress

    protected fun checkIfLoadingNow(): Boolean {
        if (progressSubj.value) return true // loading now
        progressSubj eats true
        return false
    }

    //—————————————————————————————————————————————————————————————————————— errors

    fun error(throwable: Throwable) {
        Log.e(TAG, "error: $throwable")
    }

    //—————————————————————————————————————————————————————————————————————— bindings

    protected inline fun <T> Single<T>.bind(crossinline onNext: (T) -> Unit) {
        this.subscribe({ t -> onNext.invoke(t) }, { error(it) }).addTo(disposables)
    }

    protected inline fun <T> Observable<T>.bind(crossinline onNext: (T) -> Unit) = bind(onNext, { error(it) })
    protected inline fun <T> Completable.bind(crossinline onNext: () -> Unit) = bind(onNext, { error(it) })

    protected inline fun <T> Observable<T>.bind(crossinline onNext: (T) -> Unit,
                                                crossinline onError: (Throwable) -> Unit) {
        subscribe({ onNext.invoke(it) }, { onError.invoke(it) }).addTo(disposables)
    }

    protected inline fun Completable.bind(crossinline onComplete: () -> Unit,
                                          crossinline onError: (Throwable) -> Unit) {
        subscribe({ onComplete.invoke() }, { onError.invoke(it) }).addTo(disposables)
    }
}