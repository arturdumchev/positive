package ru.anvics.positive.extentions

import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.reactivestreams.Subscriber


fun Disposable.addTo(disposable: CompositeDisposable) {
    disposable.add(this)
}


//—————————————————————————————————————————————————————————————————————— constructors
fun <T> bs() = BehaviorSubject.create<T>()

fun <T> ps() = PublishSubject.create<T>()
fun <T> bs(t: T): BehaviorSubject<T> {
    val bs = bs<T>()
    bs eats t
    return bs
}

//—————————————————————————————————————————————————————————————————————— eats operator

infix fun <T> Observer<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> Subscriber<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> BehaviorSubject<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> Emitter<T>.eats(t: T) {
    this.onNext(t)
}

infix fun <T> Emitter<T>.eats(t: Throwable) {
    this.onError(t)
}

infix fun CompletableEmitter.eats(throwable: Throwable) {
    this.onError(throwable)
}

//——————————————————————————————————————————————————————————————————————
//      REUSING SCHEDULERS
//—————————————————————————————————————————————————————————————————————— thread transformers observable

private val schedulersTransformer = ObservableTransformer<Any, Any> { upstream ->
    upstream.subscribeOn(Schedulers.io())
}

private fun <T> applySchedulers(): ObservableTransformer<T, T> {
    @Suppress("UNCHECKED_CAST")
    return schedulersTransformer as ObservableTransformer<T, T>
}

fun <T> Observable<T>.ioThreads(): Observable<T> = compose(applySchedulers())

//—————————————————————————————————————————————————————————————————————— thread transformers completable

private val schedulersTransformerC = CompletableTransformer { upstream ->
    upstream.subscribeOn(Schedulers.io())
}

fun Completable.ioThreads(): Completable = compose(schedulersTransformerC)

//—————————————————————————————————————————————————————————————————————— thread transformers single

private val schedulersTransformerS = SingleTransformer<Any, Any> { upstream ->
    upstream.subscribeOn(Schedulers.io())
}

private fun <T> applySchedulersS(): SingleTransformer<T, T> {
    @Suppress("UNCHECKED_CAST")
    return schedulersTransformerS as SingleTransformer<T, T>
}

fun <T> Single<T>.ioThreads(): Single<T> = compose(applySchedulersS())