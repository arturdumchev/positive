package ru.anvics.positive.extentions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.app.ActivityOptionsCompat
import android.transition.ChangeBounds
import android.transition.ChangeTransform
import android.transition.Slide
import android.transition.TransitionSet
import android.util.Log
import android.view.KeyCharacterMap
import android.view.KeyEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.inputmethod.InputMethodManager






fun Context.getDimen(dimenRes: Int): Int {
    return resources.getDimensionPixelSize(dimenRes)
}

fun Activity.hideKeyboard() {
    if (currentFocus == null) return
    val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    `in`.hideSoftInputFromWindow(currentFocus.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    currentFocus.clearFocus()
}

fun Context.density() = resources.displayMetrics.density
fun Context.screenWidth() = resources.displayMetrics.widthPixels
fun Context.screenHeight() = resources.displayMetrics.heightPixels

fun Context.shareText(text: String, subject: String = "") {
    val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
    sharingIntent.type = "text/plain"
    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject)
    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text)
    startActivity(Intent.createChooser(sharingIntent, "Sharing"))
}

fun Context.hasNavigationBarOnScreen(): Boolean {
    val hasMenuKey = ViewConfiguration.get(this).hasPermanentMenuKey()
    val hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK)
    Log.i("TAG", "hasNavigationBarOnScreen: hasMenuKey == $hasMenuKey, hasBackKey == $hasBackKey")
    return !hasMenuKey && !hasBackKey
}

//fun Context.hasNavBar(): Boolean {
//    val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
//    return id > 0 && resources.getBoolean(id)
//}


//—————————————————————————————————————————————————————————————————————— navigation

inline fun <reified T : Activity> Activity.navigate(sharedView: View, transitionName: String) {
    val intent = Intent(this, T::class.java)
    val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, sharedView, transitionName)
    startActivity(intent, options.toBundle())
}

class DetailsTransition : TransitionSet() {
    init {
        ordering = TransitionSet.ORDERING_TOGETHER
        addTransition(ChangeBounds())
        addTransition(ChangeTransform())
        addTransition(Slide())
    }
}
