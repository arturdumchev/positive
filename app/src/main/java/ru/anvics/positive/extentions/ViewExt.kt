package ru.anvics.positive.extentions


import android.graphics.Rect
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.RelativeLayout
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


//—————————————————————————————————————————————————————————————————————— VIEW_COMMON
fun View.rect(): Rect {
    val rect = Rect()
    getHitRect(rect)
    return rect
}

fun View.visibleNotGone(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.visibleNotInvisible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.INVISIBLE
}

fun View.onPreDraw(action: () -> Unit) {
    this.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener{
        override fun onPreDraw(): Boolean {
            viewTreeObserver.removeOnPreDrawListener(this)
            action.invoke()
            return true
        }
    })
}

//—————————————————————————————————————————————————————————————————————— VIEW_GROUP

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View
        = LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

fun <T : View> ViewGroup.onEachChild(func: (T) -> Unit) {
    for (i in 0..this.childCount - 1) {
        @Suppress("UNCHECKED_CAST") func.invoke(this.getChildAt(i) as T)
    }
}

fun ViewGroup.clearFromChildren() {
    if (childCount > 0) removeAllViews()
}



//—————————————————————————————————————————————————————————————————————— GLOBAL

fun View.statusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}


//—————————————————————————————————————————————————————————————————————— LAYOUT PARAMS
fun relativeLP(width: Number, height: Number) =
        RelativeLayout.LayoutParams(width.toInt(), height.toInt())

val relMP = RelativeLayout.LayoutParams.MATCH_PARENT
val relVC = RelativeLayout.LayoutParams.WRAP_CONTENT


val frameMP = FrameLayout.LayoutParams.MATCH_PARENT
//—————————————————————————————————————————————————————————————————————— edit text

fun EditText.observableFromEditText(): Observable<String> {
    val subj = BehaviorSubject.create<String>()
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
            subj.onNext(p0.toString())
        }
    })
    return subj
}
