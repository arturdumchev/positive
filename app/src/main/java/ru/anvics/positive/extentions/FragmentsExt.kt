package ru.anvics.positive.extentions

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

fun FragmentManager.addFrag(containerId: Int, frag: Fragment) {
    beginTransaction()
            .replace(containerId, frag)
            .addToBackStack(null)
            .commit()
}