package ru.anvics.positive.data

import android.content.SharedPreferences


class GlobalData(prefs: SharedPreferences) : PrefsDelegate(prefs), IGlobalData {
    companion object {
        private const val PRF_FIRST_ENTER = "PRF_FIRST_ENTER"
        private const val PRF_TOKEN = "PRF_TOKEN"
        private const val PRF_NAME = "PRF_NAME"
        private const val PRF_ACTIVITY_ID = "PRF_ACTIVITY_ID"
        private const val PRF_CURRENT_ACTIVITY_NAME = "PRF_CURRENT_ACTIVITY_NAME"
        private const val PRF_NEW_INVITES_COUNT = "PRF_NEW_INVITES_COUNT"

        const val BASE_STRING = ""
    }

}