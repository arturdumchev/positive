package ru.anvics.positive.data

import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


open class PrefsDelegate(protected val prefs: SharedPreferences) {

    inline protected fun <R, reified T> delegate(key: String, initial: T): ReadWriteProperty<R, T> {
        return object : ReadWriteProperty<R, T> {
            var t: T = getPrefsValue(key, initial)
            override fun getValue(thisRef: R, property: KProperty<*>): T = t
            override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
                t = value
                editPrefs(key, value)
            }
        }
    }

    inline protected fun <reified T> getPrefsValue(key: String, initial: T): T =
            when (initial) {
                is Int -> prefs.getInt(key, initial) as T
                is String -> prefs.getString(key, initial) as T
                is Long -> prefs.getLong(key, initial) as T
                is Float -> prefs.getFloat(key, initial) as T
                is Boolean -> prefs.getBoolean(key, initial) as T

                is Double -> {
                    java.lang.Double.longBitsToDouble(
                            prefs.getLong(key, java.lang.Double.doubleToLongBits(initial))) as T
                }

                else -> throw UnsupportedOperationException()
            }

    inline protected fun <reified T> editPrefs(key: String, value: T) {
        val editor: SharedPreferences.Editor
        when (value) {
            is Int -> editor = prefs.edit().putInt(key, value)
            is String -> editor = prefs.edit().putString(key, value)
            is Long -> editor = prefs.edit().putLong(key, value)
            is Float -> editor = prefs.edit().putFloat(key, value)
            is Boolean -> editor = prefs.edit().putBoolean(key, value)

            is Double -> {
                editor = prefs.edit().putLong(key, java.lang.Double.doubleToRawLongBits(value))
            }

            else -> throw UnsupportedOperationException()
        }
        editor.apply()
    }
}