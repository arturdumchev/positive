package ru.anvics.positive

import android.app.Activity
import android.content.Intent
import android.graphics.Point
import android.os.Build.VERSION_CODES.N
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.flyco.tablayout.CommonTabLayout
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_main.*
import ru.anvics.positive.extentions.hasNavigationBarOnScreen
import ru.anvics.positive.extentions.navigate
import ru.anvics.positive.feature.common.base.BaseActivity
import ru.anvics.positive.feature.detail.DetailActivity
import java.util.*


class MainActivity : BaseActivity() {

    companion object {

        private const val TAG = "MainActivity"

        const val EXTRA_LATS_POS = "EXTRA_LATS_POS"
        const val EXTRA_GOING_BACK = "EXTRA_GOING_BACK"

        private val TABS_IMAGES = listOf(R.mipmap.icon_feed_white,
                R.mipmap.icon_events_white, R.mipmap.icon_profile_white,
                R.mipmap.icon_shop_white, R.mipmap.icon_settings_white)
        private val TABS_IMAGES_SELECTED = listOf(R.mipmap.icon_feed_red,
                R.mipmap.icon_events_red, R.mipmap.icon_profile_red,
                R.mipmap.icon_shop_red, R.mipmap.icon_settings_red)

        fun startMeLikeYouGoBack(activity: Activity) {
            val intent = Intent(activity, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra(EXTRA_GOING_BACK, true)
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.enter, R.anim.stay)
        }
    }

    private class TabEntity(val icon: Int, val selIcon: Int) : CustomTabEntity {
        override fun getTabUnselectedIcon(): Int = icon
        override fun getTabSelectedIcon(): Int = selIcon
        override fun getTabTitle(): String = ""
    }

    private val mTabEntities: ArrayList<CustomTabEntity> by lazy(N) {
        ArrayList<CustomTabEntity>().apply {
            (0..TABS_IMAGES.size - 1).forEach { i ->
                this.add(TabEntity(TABS_IMAGES[i], TABS_IMAGES_SELECTED[i]))
            }
        }
    }

    fun navigate(view: View, transName: String) {
        navigate<DetailActivity>(view, transName)
    }

    fun getTabView(): CommonTabLayout = main_tab

    override val activityRoot: ViewGroup get() = main_root

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //StatusBarUtil.setColor(this, resources.getColor(R.color.colorPositiveRed))
        //StatusBarUtil.setTranslucent(this, 0)
        setContentView(R.layout.activity_main)
        translucentStatusBar()
        setUpViewPager()
        setUpTabs()

        Log.i(TAG, "onCreate: has navigationBar ${hasNavigationBarOnScreen()}")
        Log.i(TAG, "onCreate: has navigationBar2 ${hasNavBar()}")
    }


    override fun onStart() {
        super.onStart()
        //overridePendingTransition(R.anim.enter, R.anim.stay)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun setUpViewPager() {

        main_view_pager.offscreenPageLimit = 5
        main_view_pager.adapter = Adapter(supportFragmentManager)
        main_view_pager.setPagingEnabled(false)
        main_view_pager.setScrollDuration(400)
    }

    private fun setUpTabs() {
        main_tab.isIndicatorBounceEnable = false
        main_tab.setTabData(mTabEntities)
        main_tab.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                main_view_pager.currentItem = position
            }

            override fun onTabReselect(position: Int) {
            }
        })
    }
}
