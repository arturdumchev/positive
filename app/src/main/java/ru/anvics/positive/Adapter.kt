package ru.anvics.positive

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import ru.anvics.positive.feature.dummy_frag.DummyFrag
import ru.anvics.positive.feature.first_frag.FirstFrag


class Adapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    companion object {
        private val TAG = Adapter::class.java.simpleName
    }

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> FirstFrag()
        else -> DummyFrag.instance(position.getImgRes())
    }

    override fun getCount(): Int = 5


    private fun Int.getImgRes() = when (this) {
        1 -> R.drawable.frag_2
        2 -> R.drawable.frag_3
        3 -> R.drawable.frag_4
        4 -> R.drawable.frag_5
        else -> throw IllegalArgumentException()
    }
}