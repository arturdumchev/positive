package ru.anvics

import android.app.Application
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric




class App : Application(){
    companion object {
        private val TAG = App::class.java.simpleName
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
    }

}